package service

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	entity "gitlab.com/service31/Data/Entity/Quest"
)

func TestMain(m *testing.M) {
	SetupTesting()
	common.DB.AutoMigrate(&entity.PointHistory{}, &entity.Quest{}, &entity.Points{}, &entity.Quest{}, &entity.QuestProgress{})
	defer DoneTesting()
	m.Run()
}

func TestCreateQuest(t *testing.T) {
	createIn, entityID := GetNewQuestsData()

	quest, err := CreateQuest(entityID, createIn[0])

	if err != nil {
		t.Error(err)
	} else if quest.Threshold != createIn[0].GetThreshold() {
		t.Error("Threashold difference")
	}
}

func TestGetQuestsByIDs(t *testing.T) {
	var ids []uuid.UUID
	createIn, entityID := GetNewQuestsData()
	for _, r := range createIn {
		quest, err := CreateQuest(entityID, r)
		if err != nil {
			t.Error(err)
		} else if quest.Threshold != createIn[0].GetThreshold() {
			t.Error("Threashold difference")
		}
		SetupQuest(quest)
		ids = append(ids, quest.ID)
	}

	quests, err := GetQuestsByIDs(ids)

	if err != nil {
		t.Error(err)
	} else if len(quests) != len(createIn) {
		t.Errorf("Created %d but got %d", len(createIn), len(quests))
	}
}

func TestGetQuestsByIDsWithNoQuests(t *testing.T) {
	var ids []uuid.UUID

	_, err := GetQuestsByIDs(ids)

	if err == nil {
		t.Error("No error returned")
	}
}

func TestGetQuestsByEntity(t *testing.T) {
	createIn, entityID := GetNewQuestsData()
	for _, r := range createIn {
		quest, err := CreateQuest(entityID, r)
		if err != nil {
			t.Error(err)
		} else if quest.Threshold != createIn[0].GetThreshold() {
			t.Error("Threashold difference")
		}
		SetupQuest(quest)
	}

	quests, err := GetQuestsByEntity(entityID)

	if err != nil {
		t.Error(err)
	} else if len(quests) != len(createIn) {
		t.Errorf("Created %d but got %d", len(createIn), len(quests))
	}
}

func TestGetQuestsByEntityWithNoQuests(t *testing.T) {
	_, err := GetQuestsByEntity(uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestUpdateQuest(t *testing.T) {
	createIn, entityID := GetNewQuestsData()
	quest, err := CreateQuest(entityID, createIn[0])
	if err != nil {
		t.Error(err)
	} else if quest.Threshold != createIn[0].GetThreshold() {
		t.Error("Threashold difference")
	}
	SetupQuest(quest)
	updateIn := GetUpdateQuestData()

	quest, err = UpdateQuest(quest.ID, updateIn)

	if err != nil {
		t.Error(err)
	} else if quest.Description != updateIn.GetDescription() {
		t.Error("Quest description doesn't update")
	}
}

func TestUpdateQuestWithoutQuest(t *testing.T) {
	updateIn := GetUpdateQuestData()

	_, err := UpdateQuest(uuid.NewV4(), updateIn)

	if err == nil {
		t.Error("No error returned")
	}
}

func TestDeleteQuest(t *testing.T) {
	createIn, entityID := GetNewQuestsData()
	quest, err := CreateQuest(entityID, createIn[0])
	if err != nil {
		t.Error(err)
	} else if quest.Threshold != createIn[0].GetThreshold() {
		t.Error("Threashold difference")
	}
	SetupQuest(quest)

	response, err := DeleteQuest(quest.ID)

	if err != nil {
		t.Error(err)
	} else if !response {
		t.Error("Response is false")
	}
}

func TestDeleteQuestWithoutQuest(t *testing.T) {
	_, err := DeleteQuest(uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestCreateQueryProgress(t *testing.T) {
	createIn, entityID := GetNewQuestsData()
	quest, err := CreateQuest(entityID, createIn[0])
	if err != nil {
		t.Error(err)
	} else if quest.Threshold != createIn[0].GetThreshold() {
		t.Error("Threashold difference")
	}
	SetupQuest(quest)

	p, err := CreateQuestProgress(entityID, quest.ID)

	if err != nil {
		t.Error(err)
	} else if p == nil {
		t.Error("Progress is nil")
	}
}

func TestUpdateQuestProgress(t *testing.T) {
	createIn, entityID := GetNewQuestsData()
	quest, err := CreateQuest(entityID, createIn[0])
	if err != nil {
		t.Error(err)
	} else if quest.Threshold != createIn[0].GetThreshold() {
		t.Error("Threashold difference")
	}
	SetupQuest(quest)
	progress, err := CreateQuestProgress(entityID, quest.ID)
	if err != nil {
		t.Error(err)
	} else if progress == nil {
		t.Error("Progress is nil")
	}
	SetupQuestProgress(progress)

	progress, err = UpdateQuestProgress(progress.ID, 10, false, false)

	if err != nil {
		t.Error(err)
	} else if progress.CurrentProgress != 10 {
		t.Error("Failed to update quest progress")
	}
}

func TestUpdateQuestProgressWithoutQuest(t *testing.T) {
	_, err := UpdateQuestProgress(uuid.NewV4(), 10, false, false)

	if err == nil {
		t.Error("No error returned")
	}
}

func TestGetQuestInProgressByEntity(t *testing.T) {
	createIn, entityID := GetNewQuestsData()
	for _, r := range createIn {
		quest, err := CreateQuest(entityID, r)
		if err != nil {
			t.Error(err)
		} else if quest.Threshold != createIn[0].GetThreshold() {
			t.Error("Threashold difference")
		}
		SetupQuest(quest)
		p, err := CreateQuestProgress(entityID, quest.ID)
		SetupQuestProgress(p)
	}

	progresses, err := GetQuestsInProgressesByEntity(entityID)

	if err != nil {
		t.Error(err)
	} else if len(progresses) != len(createIn) {
		t.Errorf("Created %d but got %d", len(createIn), len(progresses))
	}
}

func TestGetQuestInProgressByEntityWithoutProgress(t *testing.T) {
	_, err := GetQuestsInProgressesByEntity(uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestGetQuestInProgressByID(t *testing.T) {
	createIn, entityID := GetNewQuestsData()
	quest, err := CreateQuest(entityID, createIn[0])
	if err != nil {
		t.Error(err)
	} else if quest.Threshold != createIn[0].GetThreshold() {
		t.Error("Threashold difference")
	}
	SetupQuest(quest)
	progress, err := CreateQuestProgress(entityID, quest.ID)
	if err != nil {
		t.Error(err)
	} else if progress == nil {
		t.Error("Progress is nil")
	}
	SetupQuestProgress(progress)

	progress, err = GetQuestInProgressByID(progress.ID)

	if err != nil {
		t.Error(err)
	}
}

func TestGetTotalPointsByEntityWithoutInitiate(t *testing.T) {
	p, err := GetTotalPointsByEntity(uuid.NewV4())

	if err != nil {
		t.Error(err)
	} else if p.TotalPoints != 0 {
		t.Error("Default points is not 0")
	}
}

func TestUpdatePoints(t *testing.T) {
	p, err := GetTotalPointsByEntity(uuid.NewV4())
	if err != nil {
		t.Error(err)
	} else if p.TotalPoints != 0 {
		t.Error("Default points is not 0")
	}
	SetupPoints(p)

	p, err = UpdatePoints(p.EntityID, 10, "Test")
	if err != nil {
		t.Error(err)
	} else if p.TotalPoints != 10 {
		t.Error("Points not updated")
	}
}
