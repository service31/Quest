package service

import (
	"time"

	"github.com/gogo/protobuf/types"
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	redisClient "gitlab.com/service31/Common/Redis"
	entity "gitlab.com/service31/Data/Entity/Quest"
	module "gitlab.com/service31/Data/Module/Quest"
)

//SetupTesting Init test
func SetupTesting() {
	common.BootstrapTesting(false)
}

//DoneTesting close all connections
func DoneTesting() {
	defer common.DB.Close()
	defer redisClient.RedisClient.Close()
}

//GetNewQuestsData test data
func GetNewQuestsData() ([]*module.CreateQuestRequest, uuid.UUID) {
	var requests []*module.CreateQuestRequest
	entityID := uuid.NewV4()
	from, err := types.TimestampProto(time.Now().Add(-1 * time.Hour))
	logger.CheckFatal(err)
	to, err := types.TimestampProto(time.Now().Add(1 * time.Hour))
	logger.CheckFatal(err)
	for i := 0; i < 5; i++ {
		requests = append(requests, &module.CreateQuestRequest{
			EntityID:                 entityID.String(),
			Type:                     module.QuestType_OrderAtBusiness,
			Threshold:                5000,
			RewardPoints:             100,
			Description:              common.GetRandomString(500),
			BannerImageURL:           "https://img.zh-code.com/img.png",
			AmountOfCompletionPerDay: 1,
			ActiveFrom:               from,
			ActiveTo:                 to,
			IsActive:                 true,
			IsBusinessSpecific:       false,
			IsUserSpecific:           false,
		})
	}
	return requests, entityID
}

//GetUpdateQuestData test data
func GetUpdateQuestData() *module.UpdateQuestRequest {
	from, err := types.TimestampProto(time.Now().Add(-1 * time.Hour))
	logger.CheckFatal(err)
	to, err := types.TimestampProto(time.Now().Add(1 * time.Hour))
	logger.CheckFatal(err)
	return &module.UpdateQuestRequest{
		Type:                     module.QuestType_OrderAtBusiness,
		Threshold:                5000,
		RewardPoints:             100,
		Description:              common.GetRandomString(500),
		BannerImageURL:           "https://img.zh-code.com/img.png",
		AmountOfCompletionPerDay: 1,
		ActiveFrom:               from,
		ActiveTo:                 to,
		IsActive:                 true,
		IsBusinessSpecific:       false,
		IsUserSpecific:           false,
	}
}

//SetupQuest setup quest
func SetupQuest(quest *entity.Quest) {
	bytes, err := quest.MarshalBinary()
	logger.CheckFatal(err)
	key := redisClient.RedisMapKeys.GetQuestKey(quest.ID.String())
	questMapKey := redisClient.RedisMapKeys.GetQuestMapKey(quest.EntityID.String(), quest.ID.String())
	redisClient.SetRedisValue(key, bytes)
	redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.QuestHashMap, questMapKey, key)
}

//SetupQuestProgress setup quest progress
func SetupQuestProgress(questProgress *entity.QuestProgress) {
	bytes, err := questProgress.MarshalBinary()
	logger.CheckFatal(err)
	key := redisClient.RedisMapKeys.GetQuestProgressKey(questProgress.ID.String())
	questProgressMapKey := redisClient.RedisMapKeys.GetQuestProgressMapKey(questProgress.EntityID.String(), questProgress.ID.String())
	progressOfQuestMapKey := redisClient.RedisMapKeys.GetProgressOfQuestMapKey(questProgress.AcceptedQuest.ID.String(), questProgress.ID.String())
	redisClient.SetRedisValue(key, bytes)
	redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.QuestProgressHashMap, questProgressMapKey, key)
	redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.QuestProgressHashMap, progressOfQuestMapKey, questProgress.AcceptedQuest.ID.String())
}

//SetupPoints setup points
func SetupPoints(points *entity.Points) {
	bytes, err := points.MarshalBinary()
	logger.CheckFatal(err)
	key := redisClient.RedisMapKeys.GetPointsKey(points.EntityID.String())
	redisClient.SetRedisValue(key, bytes)
}
