package service

import (
	"context"
	"fmt"
	"strings"

	"github.com/go-redis/redis/v7"
	"github.com/gogo/protobuf/types"
	"github.com/gogo/status"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	redisClient "gitlab.com/service31/Common/Redis"
	entity "gitlab.com/service31/Data/Entity/Quest"
	module "gitlab.com/service31/Data/Module/Quest"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

//CreateQuest create new Quest
func CreateQuest(entityID uuid.UUID, in *module.CreateQuestRequest) (*entity.Quest, error) {
	from, err := types.TimestampFromProto(in.GetActiveFrom())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Failed to parse time %v", in.GetActiveFrom()))
	}
	to, err := types.TimestampFromProto(in.GetActiveTo())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Failed to parse time %v", in.GetActiveTo()))
	}

	quest := &entity.Quest{
		EntityID:                 entityID,
		Type:                     module.QuestType_value[in.GetType().String()],
		Threshold:                in.GetThreshold(),
		RewardPoints:             in.GetRewardPoints(),
		Description:              strings.Trim(in.GetDescription(), " "),
		BannerImageURL:           strings.Trim(in.GetBannerImageURL(), " "),
		AmountOfCompletionPerDay: in.GetAmountOfCompletionPerDay(),
		ActiveFrom:               from,
		ActiveTo:                 to,
		IsActive:                 in.GetIsActive(),
		IsBusinessSpecific:       false,
		IsUserSpecific:           false,
		IsBusinessQuest:          in.GetIsBusinessQuest(),
		IsOncePerEntity:          in.GetIsOncePerEntity(),
	}

	if in.GetIsBusinessSpecific() {
		businessID, err := uuid.FromString(in.GetBusinessID())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "Invalid BusinessID")
		}
		quest.IsBusinessSpecific = in.GetIsBusinessSpecific()
		quest.BusinessID = businessID
	}
	if in.GetIsUserSpecific() {
		userID, err := uuid.FromString(in.GetUserID())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "Invalid UserID")
		}
		quest.IsUserSpecific = in.GetIsUserSpecific()
		quest.UserID = userID
	}

	if err = common.DB.Save(quest).Scan(&quest).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to create new Quest")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.QuestChange, quest.ID.String()))
	return quest, nil
}

//GetQuestsByIDs get quests by ids
func GetQuestsByIDs(ids []uuid.UUID) ([]*entity.Quest, error) {
	return getQuestsByIDs(ids)
}

//GetQuestsByEntity get quests by entityID
func GetQuestsByEntity(entityID uuid.UUID) ([]*entity.Quest, error) {
	return getQuestsByEntity(entityID)
}

//UpdateQuest update quest by ID
func UpdateQuest(id uuid.UUID, in *module.UpdateQuestRequest) (*entity.Quest, error) {
	quest, err := getQuestByID(id)
	if err != nil {
		return nil, err
	}
	from, err := types.TimestampFromProto(in.GetActiveFrom())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Failed to parse time %v", in.GetActiveFrom()))
	}
	to, err := types.TimestampFromProto(in.GetActiveTo())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Failed to parse time %v", in.GetActiveTo()))
	}
	quest.Type = module.QuestType_value[in.GetType().String()]
	quest.Threshold = in.GetThreshold()
	quest.RewardPoints = in.GetRewardPoints()
	quest.Description = strings.Trim(in.GetDescription(), " ")
	quest.BannerImageURL = strings.Trim(in.GetBannerImageURL(), " ")
	quest.AmountOfCompletionPerDay = in.GetAmountOfCompletionPerDay()
	quest.IsActive = in.GetIsActive()
	quest.IsOncePerEntity = in.GetIsOncePerEntity()
	quest.IsBusinessQuest = in.GetIsBusinessQuest()
	quest.ActiveFrom = from
	quest.ActiveTo = to
	if in.GetIsBusinessSpecific() {
		businessID, err := uuid.FromString(in.GetBusinessID())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "Invalid BusinessID")
		}
		quest.IsBusinessSpecific = in.GetIsBusinessSpecific()
		quest.BusinessID = businessID
	} else {
		quest.BusinessID = uuid.Nil
	}
	if in.GetIsUserSpecific() {
		userID, err := uuid.FromString(in.GetUserID())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "Invalid UserID")
		}
		quest.IsUserSpecific = in.GetIsUserSpecific()
		quest.UserID = userID
	} else {
		quest.UserID = uuid.Nil
	}
	if err = common.DB.Save(quest).Scan(&quest).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to update Quest ID: %s", id.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.QuestChange, quest.ID.String()))

	progresses := getQuestProgressesByQuestID(quest.ID)
	for _, p := range progresses {
		logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.QuestProgressChange, p))
	}
	return quest, nil
}

//DeleteQuest delete quest by ID
func DeleteQuest(ID uuid.UUID) (bool, error) {
	quest, err := getQuestByID(ID)
	if err != nil {
		return false, err
	}
	quest.IsActive = false
	if err = common.DB.Save(quest).Scan(&quest).Error; logger.CheckError(err) {
		return false, status.Error(codes.Internal, fmt.Sprintf("Failed to delete Quest ID: %s", ID.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.QuestChange, quest.ID.String()))
	progresses := getQuestProgressesByQuestID(quest.ID)
	for _, p := range progresses {
		logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.QuestProgressChange, p))
	}
	return true, nil
}

//CreateQuestProgress create quest progress
func CreateQuestProgress(entityID, questID uuid.UUID) (*entity.QuestProgress, error) {
	quest, err := getQuestByID(questID)
	if err != nil {
		return nil, err
	}

	progress := &entity.QuestProgress{
		EntityID:        entityID,
		CurrentProgress: 0,
		IsCancelled:     false,
		IsCompleted:     false,
		AcceptedQuest:   quest,
	}

	if err = common.DB.Save(progress).Scan(&progress).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to create quest progress")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.QuestProgressChange, progress.ID.String()))
	return progress, nil
}

//UpdateQuestProgress by ID
func UpdateQuestProgress(ID uuid.UUID, amount float32, isCancelled, isCompleted bool) (*entity.QuestProgress, error) {
	progress, err := getQuestProgressByID(ID)
	if err != nil {
		return nil, err
	}

	if isCancelled {
		progress.IsCancelled = true
		if err = common.DB.Save(progress).Scan(&progress).Error; logger.CheckError(err) {
			return nil, status.Error(codes.Internal, "Failed to update quest progress")
		}
		logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.QuestProgressChange, progress.ID.String()))
		return progress, nil
	}

	progress.CurrentProgress += amount

	if progress.CurrentProgress >= progress.AcceptedQuest.Threshold {
		//Completed
		points, err := getPointsByEntityID(progress.EntityID)
		if points == nil && err == nil {
			points = &entity.Points{
				EntityID:    progress.EntityID,
				TotalPoints: 0,
			}
		} else if err != nil {
			return nil, status.Error(codes.Internal, "Failed to get points")
		}
		points.TotalPoints += int64(progress.AcceptedQuest.RewardPoints)
		progress.IsCompleted = true
		err = common.DB.Transaction(func(tx *gorm.DB) error {
			if err = common.DB.Save(progress).Scan(&progress).Error; logger.CheckError(err) {
				return status.Error(codes.Internal, "Quest is completed but failed to update progress. Please try again.")
			}
			if err = common.DB.Save(points).Scan(&points).Error; logger.CheckError(err) {
				return status.Error(codes.Internal, "Quest is completed but failed to update points. Please try again.")
			}
			return nil
		})
		if err != nil {
			return nil, err
		}
		redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.PointsChange, points.EntityID.String())
		redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.QuestProgressChange, progress.ID.String())
		logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.QuestProgressChange, progress.ID.String()))
	} else {
		//Not completed
		if err = common.DB.Save(progress).Scan(&progress).Error; logger.CheckError(err) {
			return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to update progress ID: %s", ID.String()))
		}
		logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.QuestProgressChange, progress.ID.String()))
	}
	return progress, nil
}

//GetQuestsInProgressesByEntity by entityID
func GetQuestsInProgressesByEntity(entityID uuid.UUID) ([]*entity.QuestProgress, error) {
	return getQuestProgressesByEntity(entityID)
}

//GetQuestInProgressByID by ID
func GetQuestInProgressByID(ID uuid.UUID) (*entity.QuestProgress, error) {
	return getQuestProgressByID(ID)
}

//GetTotalPointsByEntity by entity ID
func GetTotalPointsByEntity(entityID uuid.UUID) (*entity.Points, error) {
	points, err := getPointsByEntityID(entityID)
	if points == nil && err == nil {
		points = &entity.Points{
			EntityID:    entityID,
			TotalPoints: 0,
		}
		if err = common.DB.Save(points).Scan(&points).Error; logger.CheckError(err) {
			return nil, status.Error(codes.Internal, "Failed to initiate points")
		}
		logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.PointsChange, entityID.String()))
	}
	return points, nil
}

//UpdatePoints by entityID
func UpdatePoints(entityID uuid.UUID, amount int64, reason string) (*entity.Points, error) {
	points, err := getPointsByEntityID(entityID)
	if err != nil {
		return nil, err
	}
	points.TotalPoints += amount
	points.History = append(points.History, &entity.PointHistory{
		EntityID:       entityID,
		AmountModified: amount,
		ModifyReason:   reason,
	})
	if err = common.DB.Save(points).Scan(&points).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to update points for Entity ID: %s", entityID.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.PointsChange, points.EntityID.String()))
	return points, nil
}

//UnaryServerAuthInterceptor user unary interceptor
func UnaryServerAuthInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		newCtx, err := common.Authenticate(ctx)
		if err != nil {
			return nil, err
		}
		return handler(newCtx, req)
	}
}

//StreamServerAuthInterceptor Quest stream interceptor
func StreamServerAuthInterceptor() grpc.StreamServerInterceptor {
	return func(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		newCtx, err := common.Authenticate(stream.Context())
		if err != nil {
			return err
		}
		wrapped := grpc_middleware.WrapServerStream(stream)
		wrapped.WrappedContext = newCtx
		return handler(srv, wrapped)
	}
}

func getQuestByID(id uuid.UUID) (*entity.Quest, error) {
	quest := &entity.Quest{}
	key := redisClient.RedisMapKeys.GetQuestKey(id.String())
	err := redisClient.GetRedisValue(key, quest)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get Quest ID: %s", id.String()))
	}
	return quest, nil
}

func getQuestsByIDs(ids []uuid.UUID) ([]*entity.Quest, error) {
	var keys []string
	var quests []*entity.Quest
	for _, id := range ids {
		keys = append(keys, redisClient.RedisMapKeys.GetQuestKey(id.String()))
	}
	vals, err := redisClient.GetRedisValues(keys)
	if err == redis.Nil {
		return quests, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get Quests")
	}
	for _, v := range vals {
		if v != nil {
			quest := &entity.Quest{}
			err = quest.UnmarshalBinary([]byte(fmt.Sprintf("%v", v)))
			if !logger.CheckError(err) {
				quests = append(quests, quest)
			}
		}
	}
	return quests, nil
}

func getQuestsByEntity(entityID uuid.UUID) ([]*entity.Quest, error) {
	var quests []*entity.Quest
	mapVals, err := redisClient.GetRedisHashScanValues(redisClient.RedisMapKeys.QuestHashMap, redisClient.RedisMapKeys.GetQuestsByEntityMapKey(entityID.String()))
	if err == redis.Nil {
		return quests, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get Quests by EntityID: %s", entityID))
	}
	vals, err := redisClient.GetRedisValues(mapVals)
	if err == redis.Nil {
		return quests, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get Quests by EntityID: %s", entityID))
	}
	for _, v := range vals {
		if v != nil {
			quest := &entity.Quest{}
			err = quest.UnmarshalBinary([]byte(fmt.Sprintf("%v", v)))
			if !logger.CheckError(err) {
				quests = append(quests, quest)
			}
		}
	}
	return quests, nil
}

func getQuestProgressByID(id uuid.UUID) (*entity.QuestProgress, error) {
	questProgress := &entity.QuestProgress{}
	key := redisClient.RedisMapKeys.GetQuestProgressKey(id.String())
	err := redisClient.GetRedisValue(key, questProgress)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get Quest Progress ID: %s", id.String()))
	}
	return questProgress, nil
}

func getQuestProgressesByEntity(entityID uuid.UUID) ([]*entity.QuestProgress, error) {
	var progresses []*entity.QuestProgress
	mapVals, err := redisClient.GetRedisHashScanValues(redisClient.RedisMapKeys.QuestProgressHashMap, redisClient.RedisMapKeys.GetQuestProgressesByEntityMapKey(entityID.String()))
	if err == redis.Nil {
		return progresses, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get Quest Progresses by EntityID: %s", entityID.String()))
	}
	vals, err := redisClient.GetRedisValues(mapVals)
	if err == redis.Nil {
		return progresses, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get Quest Progresses by EntityID: %s", entityID.String()))
	}
	for _, v := range vals {
		if v != nil {
			p := &entity.QuestProgress{}
			err = p.UnmarshalBinary([]byte(fmt.Sprintf("%v", v)))
			if !logger.CheckError(err) {
				progresses = append(progresses, p)
			}
		}
	}
	return progresses, nil
}

func getQuestProgressesByQuestID(questID uuid.UUID) []string {
	progresses, err := redisClient.GetRedisHashScanValues(redisClient.RedisMapKeys.QuestProgressHashMap,
		redisClient.RedisMapKeys.GetProgressesOfQuestByQuestKey(questID.String()))
	logger.CheckError(err)
	if progresses == nil {
		var progresses []string
		return progresses
	}
	return progresses
}

func getPointsByEntityID(entityID uuid.UUID) (*entity.Points, error) {
	points := &entity.Points{}
	key := redisClient.RedisMapKeys.GetPointsKey(entityID.String())
	err := redisClient.GetRedisValue(key, points)
	if err == redis.Nil {
		return nil, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get Points EntityID: %s", entityID.String()))
	}
	return points, nil
}
