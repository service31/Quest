package main

import (
	"context"
	"flag"
	"fmt"
	"net"
	"net/http"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	entity "gitlab.com/service31/Data/Entity/Quest"
	module "gitlab.com/service31/Data/Module/Quest"
	service "gitlab.com/service31/Quest/Service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	echoEndpoint  = flag.String("endpoint", "localhost:8000", "endpoint")
	serverChannel chan int
)

type server struct {
	module.UnimplementedQuestServiceServer
}

func (s *server) CreateQuest(ctx context.Context, in *module.CreateQuestRequest) (*module.QuestDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	quest, err := service.CreateQuest(entityID, in)
	if err != nil {
		return nil, err
	}
	dto, err := quest.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) GetQuestsByIDs(ctx context.Context, in *module.GetQuestsByIDsRequest) (*module.RepeatedQuestDTO, error) {
	var ids []uuid.UUID
	var dtos []*module.QuestDTO
	for _, id := range in.GetID() {
		i, err := uuid.FromString(id)
		if err != nil || i == uuid.Nil {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Bad ID %s", id))
		}
		ids = append(ids, i)
	}
	quests, err := service.GetQuestsByIDs(ids)
	if err != nil {
		return nil, err
	}
	for _, q := range quests {
		dto, err := q.ToDTO()
		if logger.CheckError(err) {
			return nil, status.Error(codes.Internal, fmt.Sprintf("Quest ID: %s has error. ErrorMessage: %s", q.ID.String(), err.Error()))
		}
		dtos = append(dtos, dto)
	}
	return &module.RepeatedQuestDTO{Results: dtos}, nil
}

func (s *server) GetQuestsByEntity(ctx context.Context, in *module.GetQuestsByEntityRequest) (*module.RepeatedQuestDTO, error) {
	var dtos []*module.QuestDTO
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	quests, err := service.GetQuestsByEntity(entityID)
	if err != nil {
		return nil, err
	}
	for _, q := range quests {
		dto, err := q.ToDTO()
		if logger.CheckError(err) {
			return nil, status.Error(codes.Internal, fmt.Sprintf("Quest ID: %s has error. ErrorMessage: %s", q.ID.String(), err.Error()))
		}
		dtos = append(dtos, dto)
	}
	return &module.RepeatedQuestDTO{Results: dtos}, nil
}

func (s *server) UpdateQuestByID(ctx context.Context, in *module.UpdateQuestRequest) (*module.QuestDTO, error) {
	id, err := uuid.FromString(in.GetID())
	if err != nil || id == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	quest, err := service.UpdateQuest(id, in)
	if err != nil {
		return nil, err
	}
	dto, err := quest.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) DeleteQuestByID(ctx context.Context, in *module.DeleteQuestByIDRequest) (*module.BoolResponse, error) {
	id, err := uuid.FromString(in.GetID())
	if err != nil || id == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	response, err := service.DeleteQuest(id)
	if err != nil {
		return nil, err
	}
	return &module.BoolResponse{IsSuccess: response}, nil
}

func (s *server) CreateQuestProgress(ctx context.Context, in *module.CreateQuestProgressRequest) (*module.QuestProgressDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	questID, err := uuid.FromString(in.GetQuestID())
	if err != nil || questID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad QuestID")
	}
	progress, err := service.CreateQuestProgress(entityID, questID)
	if err != nil {
		return nil, err
	}
	dto, err := progress.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) UpdateQuestProgress(ctx context.Context, in *module.UpdateQuestProgressRequest) (*module.QuestProgressDTO, error) {
	id, err := uuid.FromString(in.GetID())
	if err != nil || id == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	progress, err := service.UpdateQuestProgress(id, in.GetProgressAmount(), in.GetIsCancelled(), in.GetIsCompleted())
	if err != nil {
		return nil, err
	}
	dto, err := progress.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) GetQuestsInProgressesByEntity(ctx context.Context, in *module.GetQuestsInProgressesByEntityRequest) (*module.RepeatedQuestProgressDTO, error) {
	var dtos []*module.QuestProgressDTO
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	progresses, err := service.GetQuestsInProgressesByEntity(entityID)
	if err != nil {
		return nil, err
	}
	for _, p := range progresses {
		dto, err := p.ToDTO()
		if err != nil {
			return nil, status.Error(codes.Internal, fmt.Sprintf("Quest Progress ID: %s has error. ErrorMessage: %s", p.ID.String(), err.Error()))
		}
		dtos = append(dtos, dto)
	}
	return &module.RepeatedQuestProgressDTO{Results: dtos}, nil
}

func (s *server) GetQuestInProgressByID(ctx context.Context, in *module.GetQuestInProgressByIDRequest) (*module.QuestProgressDTO, error) {
	id, err := uuid.FromString(in.GetID())
	if err != nil || id == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	progress, err := service.GetQuestInProgressByID(id)
	if err != nil {
		return nil, err
	}
	dto, err := progress.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) GetTotalPointsByEntity(ctx context.Context, in *module.GetTotalPointsByEntityRequest) (*module.PointsDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	points, err := service.GetTotalPointsByEntity(entityID)
	if err != nil {
		return nil, err
	}
	return points.ToDTO(), nil
}

func (s *server) UpdatePoints(ctx context.Context, in *module.UpdatePointsRequest) (*module.PointsDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	points, err := service.UpdatePoints(entityID, in.GetAmount(), in.GetReason())
	if err != nil {
		return nil, err
	}
	return points.ToDTO(), nil
}

func runGRPCServer() {
	lis, err := net.Listen("tcp", ":8000")
	logger.CheckFatal(err)

	s := grpc.NewServer(
		grpc.StreamInterceptor(
			grpc_middleware.ChainStreamServer(
				grpc_validator.StreamServerInterceptor(),
				service.StreamServerAuthInterceptor(),
				grpc_zap.StreamServerInterceptor(logger.GetLogger()),
			)),
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				grpc_validator.UnaryServerInterceptor(),
				service.UnaryServerAuthInterceptor(),
				grpc_zap.UnaryServerInterceptor(logger.GetLogger()),
			)),
	)
	module.RegisterQuestServiceServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		logger.Error(fmt.Sprintf("failed to serve: %v", err))
	}

	serverChannel <- 1
}

func serveSwagger(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./Swagger/quest.swagger.json")
}

func runRestServer() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := http.NewServeMux()
	gwmux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := module.RegisterQuestServiceHandlerFromEndpoint(ctx, gwmux, *echoEndpoint, opts)
	if logger.CheckError(err) {
		serverChannel <- 999
		return
	}
	mux.Handle("/", gwmux)
	mux.HandleFunc("/swagger.json", serveSwagger)
	fs := http.FileServer(http.Dir("Swagger/swagger-ui"))
	mux.Handle("/swagger-ui/", http.StripPrefix("/swagger-ui", fs))

	logger.CheckError(http.ListenAndServe(":80", mux))
	serverChannel <- 2
}

func main() {
	common.Bootstrap()
	common.DB.AutoMigrate(&entity.PointHistory{}, &entity.Quest{}, &entity.Points{}, &entity.Quest{}, &entity.QuestProgress{})
	serverChannel = make(chan int)
	go runGRPCServer()
	go runRestServer()
	for {
		serverType := <-serverChannel
		if serverType == 1 {
			//GRPC
			logger.Info("Restarting GRPC endpoint")
			go runGRPCServer()
		} else if serverType == 2 {
			//Rest
			logger.Info("Restarting Rest endpoint")
			go runRestServer()
		} else {
			logger.Fatal("Failed to start some endPoint")
		}
	}
}
